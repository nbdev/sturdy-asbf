A basic route table-based failover system in cases where an EIP cannot be used, built around auto-scaling.

"""Packaging settings."""

from codecs import open as codec_open
from os import path
from setuptools import setup, find_packages
from sturdy_asbf import __version__

HERE = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with codec_open(path.join(HERE, 'README.rst'), encoding='utf-8') as f:
    LONG_DESCRIPTION = f.read()

setup(
    name='sturdy_asbf',

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version=__version__,

    description='A failover mechanism built via route tables (for when EIP cannot be used)',
    long_description=LONG_DESCRIPTION,

    # The project's main homepage.
    url='https://github.com/sturdy/sturdy_asbf',

    # Author details
    author='Sturdy Networks',
    author_email='hello@sturdynetworks.com',

    # Choose your license
    license='MIT',

    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Utilities',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7'
    ],

    keywords='failover route-table autoscaling',

    packages=find_packages(exclude=['docs', 'tests']),

    install_requires=['boto3', 'requests', 'docopt'],

    extras_require={
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },

    entry_points={
        'console_scripts': [
            'sturdy-asbf=sturdy_asbf.cli:main',
        ],
    },
)

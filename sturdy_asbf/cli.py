"""
sturdy-asbf

Usage:
  sturdy-asbf --route-table ROUTETABLE --cidr CIDR
  sturdy-asbf -h | --help
  sturdy-asbf --version

Options:
  -h --help                         Show this screen.
  --version                         Show version.

Examples:
  sturdy-asbf --route-table rtb-8b01ac4c --cidr 10.99.10.50/32

Note:
  Instances should have a role which allows CreateRoute, ReplaceRoute,
  DescribeRouteTables, and ModifyInstanceAttribute API calls.
"""

import boto3
import requests
import time
from docopt import docopt
from operator import itemgetter

from . import __version__ as VERSION  # pylint: disable=E0611,W0406


class App(object):
    """The whole app"""

    EC2 = boto3.client('ec2')
    EC2R = boto3.resource('ec2')
    METADATA_URL = 'http://169.254.169.254/latest/meta-data/instance-id'
    AUTO_SCALING_TAG = 'aws:autoscaling:groupName'

    def __init__(self, route_table_id, cidr):
        self.route_table_id = route_table_id
        self.cidr = cidr
        self.my_instance_id = None
        self.asg_name = None

    def setup(self):
        self.my_instance_id = requests.get(self.METADATA_URL).text
        instance = self.EC2R.Instance(self.my_instance_id)
        instance.modify_attribute(SourceDestCheck={'Value': False})

        tag = [t['Value'] for t in instance.tags if t['Key'] == self.AUTO_SCALING_TAG]

        if not tag:
            raise 'This instance is not part of an ASG'

        self.asg_name = tag[0]

        print 'Monitoring ASG %s and targetting %s @ %s as %s' % (self.asg_name, self.cidr,
                                                                  self.route_table_id, self.my_instance_id)

    def loop(self):
        """The main runloop"""

        while True:
            self.run()
            time.sleep(30)

        print "Exiting"

    def run(self):
        """A single run of the main loop"""

        paginator = self.EC2.get_paginator('describe_instances')
        page_iterator = paginator.paginate(
            Filters=[
                {
                    'Name': 'tag:aws:autoscaling:groupName',
                    'Values': [self.asg_name]
                },
                {
                    'Name': 'instance-state-name',
                    'Values': ['running']
                }
            ]
        )

        instances = list()

        for page in page_iterator:
            reservations = page['Reservations']
            instances.extend([i for r in reservations for i in r['Instances']])

        instances.sort(key=itemgetter('LaunchTime', 'PrivateIpAddress'))
        winner = instances[0]

        if winner['InstanceId'] == self.my_instance_id:
            self.win()

    def win(self):
        print "I win!"
        routeTable = self.EC2R.RouteTable(self.route_table_id)
        existingRoute = [x for x in routeTable.routes if x.destination_cidr_block == self.cidr]

        if existingRoute:
            print "Route exists"
            if existingRoute[0].instance_id != self.my_instance_id:
                print "Route goes elsewhere, replacing"
                existingRoute[0].replace(InstanceId=self.my_instance_id)
        else:
            print "Route does not exist, creating"
            routeTable.create_route(DestinationCidrBlock=self.cidr, InstanceId=self.my_instance_id)


def main():
    """Main CLI entrypoint."""
    options = docopt(__doc__, version=VERSION)
    app = App(options['ROUTETABLE'], options['CIDR'])
    app.setup()
    app.loop()
